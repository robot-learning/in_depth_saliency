function proj = project_points(models,p)

N = [models(1) models(2) models(3)]';
P = zeros(3,1);
P(1) = rand(1)*100;
P(2) = rand(1)*100;
P(3) = -(models(1)*P(1) + models(2)*P(2) + models(4))/models(3);

norm = sqrt(N'*N);
N = N/norm;

proj = zeros(size(N,1),size(p,2));
for i=1:size(proj,2)
    proj(:,i) = p(:,i) + ((P-p(:,i))' * N) * N;
end

n = sqrt(proj(:,1)'*proj(:,1));
proj = proj./n;
figure;plot3(proj(1,:),proj(2,:),proj(3,:));
hold on;
plot3([proj(1,4) proj(1,1)],[proj(2,4) proj(2,1)],[proj(3,4) proj(3,1)]);
axis equal
axis vis3d
xlabel('x')
ylabel('y')
set(gca,'ZDir','reverse');