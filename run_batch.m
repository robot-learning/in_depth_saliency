% impath = '/home/arri/Dataset/VOCB3DO/KinectColor';
impath = 'C:\Downloads\VOCB3DO';
% dpath = '/home/arri/Dataset/VOCB3DO/RegisteredDepthData';
dpath = impath;
savepath = 'C:\Downloads';
C = makecform('srgb2lab');

imlist = [1 6 10 13 32 66 69 72 77 115 117];

for i=2:length(imlist)
    i
    tic;
    I = imread(fullfile(impath,num2str(imlist(i),'img_%04d.png')));
    I_lab = applycform(I,C);
    Id = imread(fullfile(dpath,num2str(imlist(i),'img_%04d_abs.png')));
    In = computeSurfaceNormals(double(Id));
    
    I_lab_small = imresize(I_lab,0.2);
    Id_small = imresize(Id,0.2,'nearest');
    Id_small = double(Id_small)/double(max(Id_small(:)));
    In_small = imresize(In,0.2,'nearest');
    
    [S_adjust] = saliency_main(I_lab_small,Id_small,In_small,1);
    imwrite(S_adjust,fullfile(savepath,num2str(imlist(i),'sal_%04d_default.png')),'png');
    
    [S_adjust] = saliency_main(I_lab_small,Id_small,In_small,2);
    imwrite(S_adjust,fullfile(savepath,num2str(imlist(i),'sal_%04d_depth.png')),'png');
    
    [S_adjust] = saliency_main(I_lab_small,Id_small,In_small,3);
    imwrite(S_adjust,fullfile(savepath,num2str(imlist(i),'sal_%04d_normal.png')),'png');
    
    [models(i,:) support_max(i) dist] = get_plane(double(Id));
    sup = dist<0.1;
    imwrite(I.*uint8(repmat(sup,[1,1,3])),fullfile(savepath,num2str(imlist(i),'plane_%04d_1.png')),'png');
    close all;
    toc
end
save('saliency_result.mat','models','support_max');
