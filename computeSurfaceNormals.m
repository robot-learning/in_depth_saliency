function normals = computeSurfaceNormals(depth_img, rows, cols, Xs, ...
                                         Ys)
if nargin < 3
    rows = size(depth_img,1);
    cols = size(depth_img,2);
end
if nargin < 5
    Xs = [repmat(repmat([1:3],1,3),cols,1)+(repmat([1:cols].',1,9)-1)].';
    Ys = reshape(repmat([1:rows],3,1),3*rows,1);
end
normals = zeros(rows, cols, 3);
for r=2:rows-1
    r_start = (r-2)*3+1;
    Y = Ys(r_start:r_start+8);
    for c=2:cols-1
        X = Xs(:,c-1);
        Qplus = [X, Y, reshape(depth_img(r-1:r+1,c-1:c+1),9,1)];
        ni = computePatchNormal(Qplus);
        normals(r, c, :) = ni;
    end
end
n3sign = sign(normals(:,:,3));
normals(:,:,1) = normals(:,:,1).*n3sign;
normals(:,:,2) = normals(:,:,2).*n3sign;
normals(:,:,3) = normals(:,:,3).*n3sign;
end