function S = saliency_context_aware(I,Id,In,mode)
% Id = depth image
% In = normal image
% mode: 1 = color image only, 2 = color+depth, 3 = color+normal, 4 = all

size_x = size(I,2);
size_y = size(I,1);

% define patch size
offset =  2;
patch_size = (offset*2+1)^2*3;


%%% extract patches to speedup processing later
disp('Extracting patches from images');
[patches pos] = extract_patches(I,offset,patch_size);
patches_d = [];
patches_n = [];
if mode==2 || mode==4
    patches_d = extract_patches(Id,offset,patch_size/3);
end
if mode==3 || mode==4
    patches_n = extract_patches(In,offset,patch_size);
end

% compute pairwise distance between patches
disp('Computing pairwise distance between patches');
dist = zeros((size_x-2*offset)*(size_y-2*offset),size(patches,1),'single');
const = 3;
norm_pos = max(size_x,size_y);
norm_color = sqrt(255^2 *3);
norm_depth = 1;
norm_n = sqrt(3);

parfor i=1:(size_x-2*offset)*(size_y-2*offset)
    d_color = sqrt(row_sum((repmat(patches(i,:),size(patches,1),1) - patches).^2))/norm_color;
    d_pos = sqrt(row_sum((repmat(pos(i,:),size(patches,1),1) - pos).^2))/norm_pos;
    d_depth = 0;
    d_normal = 0;
    if mode==2 || mode==4
        d_depth = sqrt(row_sum((repmat(patches_d(i,:),size(patches_d,1),1) - patches_d).^2))/norm_depth;
    end
    if mode==3 || mode==4;
        d_normal = sqrt(row_sum((repmat(patches_n(i,:),size(patches_n,1),1) - patches_n).^2))/norm_n;
    end
    dist(i,:) = ((d_color+d_depth+d_normal) ./ (1+const*d_pos))';
end


% compute saliency
disp('Computing saliency');
S = zeros(1,size_y-2*offset*size_x-2*offset);
K = 64;

parfor i=1:size(dist,1)
    dist_sorted = sort(dist(i,:));
    dist_sum = row_sum(dist_sorted(1:K));
    
    S(i) = 1 - exp(-dist_sum/K);
end
S = reshape(S,[size_y-2*offset size_x-2*offset]);
figure;imshow(S);
