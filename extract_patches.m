function [patches pos] = extract_patches(I,offset,patch_size)

% create the image in 2 additional scales
I2 = imresize(I,0.5);
I3 = imresize(I,0.25);

size_x = size(I,2);
size_y = size(I,1);
size_x2 = size(I2,2);
size_y2 = size(I2,1);
size_x3 = size(I3,2);
size_y3 = size(I3,1);

patches = zeros((size_x-2*offset)*(size_y-2*offset)+(size_x2-2*offset)*(size_y2-2*offset)+(size_x3-2*offset)*(size_y3-2*offset),patch_size,'single');
pos = zeros((size_x-2*offset)*(size_y-2*offset),2,'single');
count = 0;
for i=1+offset:size(I,1)-offset
    for j=1+offset:size(I,2)-offset
        idx = (j-1-offset) * (size_y-2*offset) + (i-offset);
        patches(idx,:) = reshape(I(i-offset:i+offset,j-offset:j+offset,:),1,patch_size);
        pos(idx,:) = [i-offset j-offset];
    end
end
count = (size_x-2*offset)*(size_y-2*offset);
for i=1+offset:size(I2,1)-offset
    for j=1+offset:size(I2,2)-offset
        idx = (j-1-offset) * (size_y2-2*offset) + (i-offset);
        idx = idx + count;
        patches(idx,:) = reshape(I2(i-offset:i+offset,j-offset:j+offset,:),1,patch_size);
        pos(idx,:) = [(i-offset)*2 (j-offset)*2];
    end
end
count = count + (size_x2-2*offset)*(size_y2-2*offset);
for i=1+offset:size(I3,1)-offset
    for j=1+offset:size(I3,2)-offset
        idx = (j-1-offset) * (size_y3-2*offset) + (i-offset);
        idx = idx + count;
        patches(idx,:) = reshape(I3(i-offset:i+offset,j-offset:j+offset,:),1,patch_size);
        pos(idx,:) = [(i-offset)*4 (j-offset)*4];
    end
end