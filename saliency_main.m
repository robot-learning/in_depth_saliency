function [S_adjust S_all] = saliency_main(I,Id,In,mode)
% I is image in Lab colorspace. To convert RGB to Lab, do:
%   C = makecform('srgb2lab');
%   I_lab = applycform(Irgb,C);
% 
% Run the code with small-ish (~100x100) I if you don't want to wait too
% long.
% This code uses matlab parallel processing
% execute 'matlabpool' before running the code

if nargin<2
    mode = 1;
    Id = 0;
    In = 0;
end

% create different scales of image
I2 = imresize(I,0.8);
I3 = imresize(I,0.5);
I2d = imresize(Id,0.8);
I3d = imresize(Id,0.5);
I2n = imresize(In,0.8);
I3n = imresize(In,0.5);

disp('Computing saliency for scale 1');
S = saliency_context_aware(I,Id,In,mode);
disp('Computing saliency for scale 2');
S2 = saliency_context_aware(I2,I2d,I2n,mode);
disp('Computing saliency for scale 3');
S3 = saliency_context_aware(I3,I3d,I3n,mode);

S2_res = imresize(S2,size(S),'nearest');
S3_res = imresize(S3,size(S),'nearest');

S_all = (S + S2_res + S3_res)/3;
figure;imshow(S_all);

S_foci = S_all > 0.2;
if sum(S_foci(:))<10
    S_foci = S_all > (max(S_all(:))*0.35);
end

% % offset is patch_size/2 -1
% offset = 2;
x_coord = repmat(1:size(S,2),size(S,1),1);
y_coord = repmat([1:size(S,1)]',1,size(S,2));


norm = sqrt((size(S,1)-1)^2 + (size(S,2)-1)^2);

% for every pixel, find the closest foci
disp('Reweighting saliency based on focus of attention');
S_adjust = S_all;
for i=1:size(S_all,1)
    for j=1:size(S_all,2)
        if ~S_foci(i,j)
            pixel_dist = sqrt((i-y_coord).^2 + (j-x_coord).^2);
            [dist idx] = sort(pixel_dist(:));
            sorted_foci = S_foci(idx);
            foci = find(sorted_foci,1);

            S_adjust(i,j) = S_all(i,j) * (1-dist(foci)/norm);
        end
    end
end
S_all = S_all ./ max(max(S_all(:),0.1));
S_adjust = S_adjust ./ max(max(S_adjust(:),0.1));
figure;imshow(S_adjust);
disp('End');