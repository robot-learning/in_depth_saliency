function ni = computePatchNormal(Qplus)
qplus_bar = mean(Qplus);
% Qplus_bar = repmat(qplus_bar, size(Qplus,1), 1);
Qplus_bar = qplus_bar([1:size(qplus_bar,1)]' * ones(1,9),  :);
[u,s,v] = svd(Qplus - Qplus_bar);
ni = v(:,end);
end