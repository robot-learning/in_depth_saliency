function [models support_max dist_max] = get_plane(Id)

size_x = size(Id,2);
size_y = size(Id,1);

pos_x = repmat(1:size_x,size_y,1);
pos_y = -repmat([1:size_y]',1,size_x);

models = [];
support_max = 0;
dist_max = [];


thres = 0.05;
d = 1;
for i=1:1000
    while (true)
        x = min(size_x,round(rand(3,1)*size_x)+1);
        y = max(-size_y,-round(rand(3,1)*size_y)-1);
        z = [Id(abs(y(1)),x(1)); Id(abs(y(2)),x(2)); Id(abs(y(3)),x(3))];
        if sum(z~=0)==3
            break;
        end
    end
    
    D = det([x(1) y(1) z(1); x(2) y(2) z(2); x(3) y(3) z(3)]);
    a = -d * det([1 y(1) z(1); 1 y(2) z(2); 1 y(3) z(3)]) / D;
    b = -d * det([x(1) 1 z(1); x(2) 1 z(2); x(3) 1 z(3)]) / D;
    c = -d * det([x(1) y(1) 1; x(2) y(2) 1; x(3) y(3) 1]) / D;
    
    dist = abs(a*pos_x + b*pos_y + c*Id + d);
    
    support = sum(dist(:)<thres);
    if support > support_max
        models = [a b c d];
        support_max = support;
        dist_max = dist;
    end
end
support_max
support = dist_max < thres;
support = support(:);
if sum(support) > 2000
    [a sorted] = sort(dist_max(:));
    support(:) = 0;
    support(sorted(1:2000)) = 1;
end
points = [pos_x(support(:))';pos_y(support(:))';Id(support(:))'];
centroid = sum(points,2)/size(points,2);
A = points - repmat(centroid,1,size(points,2));
[U S V] = svd(A);
models(1:3) = U(:,3)';

% f = repmat(-d,1,size(points,2));
% models(1:3) = (points/f)';
